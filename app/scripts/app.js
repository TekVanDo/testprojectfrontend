'use strict';

/**
 * @ngdoc overview
 * @name testProjectFrontendApp
 * @description
 * # testProjectFrontendApp
 *
 * Main module of the application.
 */
angular
  .module('testProjectFrontendApp', [
    'ngAnimate',
    'mgcrea.ngStrap'
  ]);
