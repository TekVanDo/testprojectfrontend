'use strict';

/**
 * @ngdoc function
 * @name testProjectFrontendApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the testProjectFrontendApp
 */
angular.module('testProjectFrontendApp')
  .controller('MainCtrl', function ($http,$modal,$q,$scope) {
    var app = this;
    var modal = $modal({scope:$scope,title: 'detail view',contentTemplate: 'views/detailView.html', show: false});
    this.moreThenDate = false;
    this.lessThenDate = false;
    this.requestCollection = [];
    this.selected = false;

    this.getData = function () {
        $http.get('http://testProjectBackend?action=getAll').
            success(function(data) {
                if(data.data != undefined && data.error.length == 0){
                    app.requestCollection = data.data;
                }
            }).error(function() {

            });
    };

    this.showInfo = function (object) {
        this.selected = object;
        modal.$promise.then(modal.show);
    };

    this.dateFilter = function (item) {
        if(app.lessThenDate.fields && app.moreThenDate.fields){
            if(item.fields.requestTime >= app.moreThenDate.fields.requestTime && item.fields.requestTime <= app.lessThenDate.fields.requestTime){
                return item;
            }else{
                return false;
            }
        }
        return  item;
    };

    this.init = function () {
        this.getData();
    };
    this.init();
  });
