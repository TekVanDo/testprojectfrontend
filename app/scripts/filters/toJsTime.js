/**
 * Created by Tek on 20.10.2014.
 */
angular.module('testProjectFrontendApp').filter('tojstime', function() {
    return function (input) {
        return input * 1000;
    }
});